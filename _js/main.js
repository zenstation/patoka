$(document).ready(function () {
    if($(this).scrollTop()>0){
        $('.js-head').addClass('b-header--fixed');
    }
    $(window).scroll(function(){
        if($(this).scrollTop()>0){
            $('.js-head').addClass('b-header--fixed');
        }
        else if ($(this).scrollTop()===0){
            $('.js-head').removeClass('b-header--fixed');
        }
    });

    $(".js-to-form").click(function () {
        $('html, body').stop().animate({
            scrollTop: 150}, 500);
        return false;
    });




    $('.js-collapse-block').on('hide.bs.collapse', function () {
        $(this).parent().find('.js-filter-arrow').removeClass('open')
    })
    $('.js-collapse-block').on('show.bs.collapse', function () {
        $(this).parent().find('.js-filter-arrow').addClass('open');

    });


    if($('.js-main-slider').length) {
        var _indexSlideWrap = $('.js-main-slider');
        var _indexSlide = new Swiper(_indexSlideWrap, {
            loop: true,
            speed: 1200,
            nextButton: _indexSlideWrap.find('.b-slider__arrow--next'),
            prevButton: _indexSlideWrap.find('.b-slider__arrow--prev'),
            parallax: true,
            autoplay: false,
        });
    };


    if($('.js-img-popup').length) {
        $('.js-img-popup').magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            mainClass: 'mfp-img-mobile',
            image: {
                verticalFit: true
            }
        });
    }
    if($('.js-slider-thumbs-popup').length) {
        $('.js-slider-thumbs-popup').magnificPopup({
            delegate: 'a',
            type: 'image',
            tLoading: 'Loading image #%curr%...',
            mainClass: 'mfp-img-mobile',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0,1],
                tCounter: '<span class="mfp-counter hidden">%curr% of %total%</span>'
            }
        });
    }

    $('#nav_bar .col .item').click(function () {
        var _sec_name = $(this).attr('data-section-name');
        $.scrollify.move("#"+_sec_name);
    });

    $('#rusbtn').on('click', function() {
        $('.default-country').css('fill','#e3e3e3');
        $('#rus').css('fill','#f89e00');
        $('.country-btn').css('color','#444444');
        $('#rusbtn').css('color','#f89e00');
    });
    $('#belbtn').on('click', function() {
        $('.default-country').css('fill','#e3e3e3');
        $('#bel').css('fill','#f89e00');
        $('.country-btn').css('color','#444444');
        $('#belbtn').css('color','#f89e00');

    });
    $('#kazbtn').on('click', function() {
        $('.default-country').css('fill','#e3e3e3');
        $('#kaz').css('fill','#f89e00');
        $('.country-btn').css('color','#444444');
        $('#kazbtn').css('color','#f89e00');

    });
    $('#kirbtn').on('click', function() {
        $('.default-country').css('fill','#e3e3e3');
        $('#kir').css('fill','#f89e00');
        $('.country-btn').css('color','#444444');
        $('#kirbtn').css('color','#f89e00');

    });
    $('#estbtn').on('click', function() {
        $('.default-country').css('fill','#e3e3e3');
        $('#est').css('fill','#f89e00');
        $('.country-btn').css('color','#444444');
        $('#estbtn').css('color','#f89e00');

    });
    $('#litbtn').on('click', function() {
        $('.default-country').css('fill','#e3e3e3');
        $('#lit').css('fill','#f89e00');
        $('.country-btn').css('color','#444444');
        $('#litbtn').css('color','#f89e00');

    });
    (function($) {
        $(function() {

            $('input, select').styler();

        });
    })(jQuery);


    $(document).ready(function(){
        $('.js-frontslider').slick({
            arrows:false,
            autoplay:true,
            dots:true,
            dotsClass:true,
        });
    });

});

$(window).on('load', function () {
    if($('.js-men-section').length) {
        $.scrollify({
            section : ".js-men-section",
            sectionName : "section-name",
            interstitialSection : ".b-footer, .b-head-img",
            easing: "easeOutExpo",
            scrollSpeed: 1100,
            offset : 0,
            scrollbars: true,
            standardScrollElements: "",
            setHeights: true,
            overflowScroll: true,
            updateHash: true,
            touchScroll:true,
            before:function() {
                console.log($.scrollify.current().hasClass('js-interstitial'));
                if (!$.scrollify.current().hasClass('js-interstitial')) {
                    if ($('.js-nav-section').is(':hidden')) {
                        $('.js-nav-section').fadeIn();
                    }
                    setActiveNavbar($.scrollify.current().attr('data-section-name'));
                } else {
                    $('.js-nav-section').fadeOut(100);
                }
            },
            after:function() {

            },
            afterResize:function() {
                $.scrollify.update();
            },
            afterRender:function() {
                if ($.scrollify.current().hasClass('js-interstitial')) {
                    $('.js-nav-section').fadeOut(100);
                }
            }
        });
    }
});


function truncated(num, decimalPlaces) {
    var numPowerConverter = Math.pow(10, decimalPlaces);
    return ~~(num * numPowerConverter)/numPowerConverter;
}


function bildNavbar() {
    var _mas = $('div[id^="team"]');
    for (var i = 0; i < _mas.length; i++) {
        $('#nav_bar .col').append('<div class="item" data-section-name=""><span></span></div>');
        $('#nav_bar .col .item').last().attr('data-section-name', $(_mas[i]).attr('data-section-name'));
    }
}
function setActiveNavbar(attr) {
    console.log(attr);
    $('.js-nav-item').removeClass('b-nav-section__item--active');
    $('.js-nav-item[data-section-name='+attr+']').addClass('b-nav-section__item--active');
}

