var gulp = require('gulp'),
    watch = require('gulp-watch'),
    postcss = require('gulp-postcss'),
    autoprefixer = require('autoprefixer'),
    sass = require('gulp-sass'),
    cssnano = require('gulp-cssnano'),
    uglify = require('gulp-uglify'),
    cheerio = require('gulp-cheerio'),
    sprite = require('gulp-svg-sprite'),
    svgmin = require('gulp-svgmin'),
    replace = require('gulp-replace'),
    rename = require('gulp-rename'),
    print = require('gulp-print');
var path = {
    build: {
        js: '_js/',
        css: '_css/',
    },
    src: {
        js: ['_js/**/*.js', '!_js/**/*.min.js', '!_js/**/*-min.js'],
        style: '_css/**/*.scss',
    },
    watch: {
        js: ['_js/**/*.js', '!_js/**/*.min.js', '!_js/**/*-min.js'],
        style: '_css/**/*.scss',
    }
};
gulp.task('js:build', function () {
    return gulp.src(path.src.js)
        .pipe(uglify()).on("error", function () {
            console.log('ERROR JS')
        })
        .pipe(rename(function (path) {
            if (path.extname === '.js') {
                path.basename += '.min';
            }
        }))
        .pipe(gulp.dest(path.build.js))
});

gulp.task('style:build', function () {
    return gulp.src(path.src.style)
        .pipe(sass()).on("error", sass.logError)
        .pipe(postcss([autoprefixer({browsers: ['>0%']})]))
        .pipe(cssnano({autoprefixer: false, convertValues: false, zindex: false}))
        .pipe(rename(function (path) {
            if (path.extname === '.css') {
                path.basename += '.min';
            }
        }))
        .pipe(gulp.dest(path.build.css))
});


// запускаем 1 раз после кастомизации параметров
gulp.task('bootstrap', function () {
    return gulp.src('_plugins/bootstrap/scss/bootstrap.scss')
        .pipe(sass()).on("error", sass.logError)
        .pipe(postcss([autoprefixer({browsers: ['>0%']})]))
        .pipe(cssnano({autoprefixer: false, convertValues: false, zindex: false}))
        .pipe(rename(function (path) {
            if (path.extname === '.css') {
                path.basename += '.min';
            }
        }))
        .pipe(gulp.dest('_css/'))
});

gulp.task('svgSpriteBuild', function () {
    return gulp.src('_img/svg/*.svg')
        .pipe(svgmin({
            js2svg: {
                pretty: true
            }
        }))
        .pipe(cheerio({
            run: function ($) {
                $('[fill]').removeAttr('fill');
                $('[stroke]').removeAttr('stroke');
                $('[style]').removeAttr('style');
            },
            parserOptions: {xmlMode: true}
        }))
        .pipe(replace('&gt;', '>'))
        .pipe(sprite({
            mode: {
                symbol: {
                    sprite: "../sprite.svg",
                }
            }
        }))
        .pipe(gulp.dest('_img/'));
});


gulp.task('build', gulp.parallel('js:build', 'style:build', 'svgSpriteBuild'));
gulp.task('watch', function () {
    watch([path.watch.style], gulp.parallel("style:build"));
    watch(path.watch.js, gulp.parallel("js:build"));
});

gulp.task('default', gulp.parallel('build', 'watch'));