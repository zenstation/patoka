<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>honey</title>
    <link rel="stylesheet" href="/_css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
    <link rel="stylesheet" href="/_plugins/slick/slick.css">
    <link rel="stylesheet" href="/_plugins/slick/slick-theme.css">
    <link href="/_plugins/formstyler/jquery.formstyler.css" rel="stylesheet"/>
    <link href="/_plugins/formstyler/jquery.formstyler.theme.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/_css/main.min.css">
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light">
    <a class="navbar-brand" href="#"><img src="../_img/head-logo.png"></a>
        <button class="navbar-toggler navbar-toggler-right collapsed" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02">
        <span class="navbar-toggler-menu">Меню</span>
        <span class="rotate"> </span>
        <span class="rotate"> </span>
        <span class="rotate"> </span>
    </button>
    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
        <ul class="navbar-nav mr-auto mt-auto mt-lg-0">
            <li class="nav-item nav-item__phone">
                <a class="nav-link" href="tel:+74942655-791">+7 (4942) 655-791</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Продукция</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">О&nbsp;заводе</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Логистика</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Поставщикам</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Контакты</a>
            </li>
        </ul>
        <div class="collapse-btnblock my-2 my-lg-0">
            <div class="collapse-btnblock-drop dropdown">
                <button class="collapse-btnblock-drop__btn btn btn-secondary dropdown-toggle text-left" type="button"
                        id="dropdownMenuButton"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Ru
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="#">Eng</a>
                    <a class="dropdown-item" href="#">Gem</a>
                    <a class="dropdown-item" href="#">Ita</a>
                </div>
            </div>
            <div class="collapse-btnblock-wide">
                <button type="button" class="collapse-btnblock-wide__btn btn text-center">Сотрудничать</button>
            </div>
        </div>

    </div>
</nav>
