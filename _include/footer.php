<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-sm-3 col-12 footer-col footer-col1">
                <div class="footer-logoblock"><img class="footer-logoblock__logo" src="../_img/footer-logo.png"></div>

            </div>
            <div class="col-md-6 col-sm-6 col-12 footer-col footer-col2">
                <nav class="nav footer-menu">
                    <a class="nav-link footer-menu__btn" href="#">Продукция</a>
                    <a class="nav-link footer-menu__btn" href="#">О заводе</a>
                    <a class="nav-link footer-menu__btn" href="#">Логистика</a>
                    <a class="nav-link footer-menu__btn" href="#">Поставщикам</a>
                    <a class="nav-link footer-menu__btn" href="#">Контакты</a>
                </nav>
            </div>
            <div class="col-md-3 offset-md-1 col-sm-3 col-12 footer-col footer-col3">
                <div class="footer-col-block">
                    <h4 class="footer-col-block__h">Костромская область, п. Паточного завода</h4>
                    <p class="footer-col-block__p">
                        <a class="cooperate-contacts-num__a" href="tel:+74942668-104">+7 (4942) 668-104</a>,
                        <a class="cooperate-contacts-num__a" href="tel:+74942655-791">+7 (4942) 655-791</a><br>
                        <a href="mailto:Info@koskpz.ru">Info@koskpz.ru</a>,
                        <a href="mailto:market@koskpz.ru">market@koskpz.ru</a>
                    </p>
                </div>
            </div>
            <div class="col-md-2 col-md-2 col-sm-3 col-12 footer-col footer-col4 ">
                <div class="under-footer-made-by">
                    <h5 class="under-footer-made-by__media">Сделали в</h5><a href="#"> «Медиасеть»</a>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-12 footer-col footer-col5">
                <div class="under-footer-factory">
                    <h5 class="under-footer-factory__name">ООО «КОСТРОМСКОЙ КРАХМАЛО-ПАТОЧНЫЙ ЗАВОД». <span
                                class="under-footer-factory__name__law">Все права защищены</span></h5>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="/_js/jquery-3.1.0.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script src="/_plugins/formstyler/jquery.formstyler.min.js"></script>
<script type="text/javascript" src="/_plugins/slick/slick.min.js"></script>
<script src="/_js/main.min.js"></script>
</body>
</html>